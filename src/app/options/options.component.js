const HTML = `
 <div class="container-xxl mt-3">
    <div class="row">
        <div class="col">
            <h2><img src="../../assets/image/lum_logo.png" style="width: 50px; vertical-align: middle"> <span>lum extension settings</span></h2>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <table class="table">
                <tbody>
                <tr>
                    <td width="20%">
                        <b>User wallet</b>
                    </td>
                    <td>
                        <div class="mb-3">
                            <label for="lum-wallet-address" class="form-label">lum wallet address</label>
                            <input type="text" class="form-control" id="lum-wallet-address" aria-describedby="lum-wallet-address-help">
                            <div id="lum-wallet-address-help" class="form-text">We'll never save your lum wallet address.</div>
                            <div class="valid-feedback">
                               <b>Lum address is valid</b>
                            </div>
                            <div class="invalid-feedback">
                                <b>Lum address is not valid</b>
                            </div>
                            <div class="validation-inprogress-feedback">
                                <b>Validation in progress</b> <lum-loader lum-loader-width = "10" lum-loader-color="#cb5f2e"></lum-loader>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <div class="mb-3 mt-3">
                            <b>Notification</b>
                        </div>
                       
                    </td>
                    <td>
                        <div class="mb-3 mt-3" >
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="notif-limit-rewards" disabled>
                                <label class="form-check-label" for="notif-limit-rewards">Send a notification when the limit of lum in rewards is reached</label>
                            </div>
                        </div>
                        <div class="mb-3 mt-3" style="margin-left: 4%; margin-right: 50%">
                            <label for="notif-limit-rewards-number" class="form-label" >limit of lum in rewards</label>
                            <div class="input-group mb-3">
                                <input type="number" disabled class="form-control" id="notif-limit-rewards-number" value="500" aria-label="Recipient's username" aria-describedby="input-extension-lum" disabled>
                                <span class="input-group-text" id="input-extension-lum">lum</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="display: none">
                        <button class="btn btn-primary" id="clear-cache">Clear cache</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
.validation-progress~.validation-inprogress-feedback{
    display: block;
}

.validation-inprogress-feedback{
    display: none;
    width: 100%;
    margin-top: 0.25rem;
    font-size: .875em;
    color: #cb5f2e;
}
</style>
`

import LocalStorageService from "../shared/service/local-storage/local-storage.service.js";
import LumNetworkService from "../shared/service/lum-network/lum-network.service.js";
import BaseComponent from "../shared/component/base.component.js";
import LoaderComponent from "../shared/component/loader/loader.component.js";

export default class OptionsComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        this.template.innerHTML = OptionsComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.shadowRoot.getElementById('lum-wallet-address').onchange = e => this.onChange(e);
        this.shadowRoot.getElementById('notif-limit-rewards').onchange = e => this.onChange(e);
        this.shadowRoot.getElementById('notif-limit-rewards-number').onchange = e => this.onChange(e);
        this.shadowRoot.getElementById('clear-cache').onclick= e => LocalStorageService.clear();
        this.loadOptionsValues();
    }

    async onChange(e) {

        if(e.path[0].id === "lum-wallet-address"){
            LumNetworkService.clear();
            await this.validateLumAddress(e.path[0].value);
            await LocalStorageService.saveObject({"lum_address":e.path[0].value});
        }

        if(e.path[0].id === "notif-limit-rewards"){
            if(e.path[0].checked){
                this.shadowRoot.getElementById('notif-limit-rewards-number').removeAttribute('disabled');
                await LocalStorageService.saveObject({"notif-limit-rewards":true});
            }else{
                this.shadowRoot.getElementById('notif-limit-rewards-number').setAttribute('disabled',"");
                await LocalStorageService.saveObject({"notif-limit-rewards":false});
            }
        }
        if(e.path[0].id === "notif-limit-rewards-number"){
            await LocalStorageService.saveObject({"notif-limit-rewards-number":e.path[0].value});
        }
    }

    async loadOptionsValues() {
        const lumWalletAddress = await LocalStorageService.getObject("lum_address");
        const notifications = {
            rewards: {
                active: await LocalStorageService.getObject("notif-limit-rewards"),
                limit : await LocalStorageService.getObject("notif-limit-rewards-number") !== undefined ? await LocalStorageService.getObject("notif-limit-rewards-number") : 500,
            }
        };
        await this.render(lumWalletAddress,notifications);
    }

    async render(lumWalletAddress, notifications) {
        this.shadowRoot.getElementById('notif-limit-rewards').checked = notifications.rewards.active;
        if(notifications.rewards.active){
            this.shadowRoot.getElementById('notif-limit-rewards').removeAttribute('disabled');
            this.shadowRoot.getElementById('notif-limit-rewards-number').removeAttribute('disabled');
        }else{
            this.shadowRoot.getElementById('notif-limit-rewards-number').setAttribute('disabled',"");
        }

        this.shadowRoot.getElementById('notif-limit-rewards-number').value= notifications.rewards.limit;
        if(lumWalletAddress !== undefined){
            this.shadowRoot.getElementById("lum-wallet-address").value = lumWalletAddress;
            if(await LocalStorageService.getObject("lum_address_is_valid")){
                this.shadowRoot.getElementById("lum-wallet-address").classList.add('is-valid');
                this.shadowRoot.getElementById("lum-wallet-address").classList.remove('is-invalid');
                this.shadowRoot.getElementById("lum-wallet-address").classList.remove('validation-progress');
            }else{
                await this.validateLumAddress(lumWalletAddress);
            }
        }
    }

    async validateLumAddress(lumWalletAddress) {
        this.shadowRoot.getElementById("lum-wallet-address").classList.add('validation-progress');
        this.shadowRoot.getElementById("lum-wallet-address").classList.remove('is-invalid');
        this.shadowRoot.getElementById("lum-wallet-address").classList.remove('is-valid');
        const lumUserWallet = await LumNetworkService.getLumUserWalletInfo(lumWalletAddress);
        if(lumUserWallet === undefined){
            await LocalStorageService.saveObject({"lum_address_is_valid":false});
            this.shadowRoot.getElementById("lum-wallet-address").classList.add('is-invalid');
            this.shadowRoot.getElementById("lum-wallet-address").classList.remove('is-valid');
            this.shadowRoot.getElementById("lum-wallet-address").classList.remove('validation-progress');
            this.shadowRoot.getElementById('notif-limit-rewards-number').setAttribute('disabled',"");
            this.shadowRoot.getElementById('notif-limit-rewards').setAttribute('disabled',"");
        }else{
            await LocalStorageService.saveObject({"lum_address_is_valid":true});
            this.shadowRoot.getElementById("lum-wallet-address").classList.add('is-valid');
            this.shadowRoot.getElementById("lum-wallet-address").classList.remove('is-invalid');
            this.shadowRoot.getElementById("lum-wallet-address").classList.remove('validation-progress');
            this.shadowRoot.getElementById('notif-limit-rewards').removeAttribute('disabled');
            if(await LocalStorageService.getObject("notif-limit-rewards")){
                this.shadowRoot.getElementById('notif-limit-rewards-number').removeAttribute('disabled');
            }
        }
    }
}

window.customElements.define('lum-options', OptionsComponent);

