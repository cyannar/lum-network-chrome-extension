const HTML = `
<lum-info></lum-info>
<hr>
<div id="dashboard-ok"></div>
<div id="dashboard-ko" style="text-align: center">  
   Lum address in undefined or invalid. Provide it in <a id="option-link" href="#" onclick="">option</a> pane 
</div>
<hr>
<lum-footer></lum-footer>
`
import LumInfoComponent from "../shared/component/lum-info/lum-info.component.js";
import DashboardComponent from "../shared/component/dashboard/dashboard.component.js";
import FooterComponent from "../shared/component/footer/footer.component.js";
import LumNetworkService from "../shared/service/lum-network/lum-network.service.js";
import LocalStorageService from "../shared/service/local-storage/local-storage.service.js";
import BaseComponent from "../shared/component/base.component.js";
import ExchangeRateService from "../shared/service/exchange-rate/exchange-rate.service.js";

export default class PopupComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        this.template.innerHTML = PopupComponent.getStyleSheets() + HTML;
    }

    async connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));

        this.shadowRoot.getElementById("option-link").onclick = e => chrome.runtime.openOptionsPage();

        if (await LocalStorageService.getObject('lum_address') === undefined
            || await LocalStorageService.getObject('lum_address') === ""
            || !await LocalStorageService.getObject('lum_address_is_valid')
        )
        {
            this.shadowRoot.getElementById("dashboard-ok").style.display = 'none';
            this.shadowRoot.getElementById("dashboard-ko").style.display = 'block';
        }else{
            const lum_dashboard = document.createElement('lum-dashboard');
            this.shadowRoot.getElementById("dashboard-ok").appendChild(lum_dashboard)
            this.shadowRoot.getElementById("dashboard-ok").style.display = 'block';
        }
    }

    async attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            LumNetworkService.clear();
            ExchangeRateService.clear();

            this.shadowRoot.querySelector("lum-info").setAttribute("refresh-all", "true");
            if(await LocalStorageService.getObject('lum_address_is_valid')){
                this.shadowRoot.querySelector("lum-dashboard").setAttribute("refresh-all", "true");
            }

        }
        if (attr === "refresh-currency") {
            ExchangeRateService.clear();

            this.shadowRoot.querySelector("lum-info").setAttribute("refresh-all", "true");
            if(await LocalStorageService.getObject('lum_address_is_valid')){
                this.shadowRoot.querySelector("lum-dashboard").setAttribute("refresh-all", "true");
            }
        }
    }
}

window.customElements.define('lum-popup', PopupComponent);
