const template = document.createElement('template');
const HTML = "";
export default class BaseComponent extends HTMLElement {

    static styleSheets = [];
    static attributes = [
        'refresh-all',
        'refresh-currency'
    ];
    static styles = {
        'bootstrap': '/src/lib/bootstrap-5.0.2/css/bootstrap.min.css',
        'popup': '/src/app/popup/popup.css'
    };

    static childComponents = [];

    constructor(){
        super();
        this.template = template
    }

    static getStyleSheets(){
        let html_import = "";
        let test = this;
        this.styleSheets.forEach(function (styleSheet){
            html_import += '<link rel="stylesheet" href="' + test.styles[styleSheet] + '">';
        });
        return html_import;
    }

    static get observedAttributes() {return this.attributes; }

    formatPriceToDisplay(price, currency, maximumFractionDigits)
    {
        let formatter = new Intl.NumberFormat(undefined, {
            style: 'currency',
            currency: currency,
            maximumFractionDigits : maximumFractionDigits
        });
        return formatter.format(price);
    }

}
