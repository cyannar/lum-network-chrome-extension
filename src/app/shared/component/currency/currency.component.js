const HTML = `
<select class="form-select form-select-sm" id="currency" name="currency" style="border-color: #6c757d;">
    <option value="USD" selected>$US</option>
    <option value="EUR">&euro;</option>
</select>
`
import LocalStorageService from "../../service/local-storage/local-storage.service.js";
import BaseComponent from "../base.component.js";

export default class CurrencyComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup'
    ];

    constructor() {
        super();
        this.template.innerHTML = CurrencyComponent.getStyleSheets() + HTML;
    }

    async connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.shadowRoot.getElementById('currency').onchange = e => this.onChange(e);
        this.shadowRoot.getElementById("currency").value = await LocalStorageService.getObject("currency") === undefined ? "USD" : await LocalStorageService.getObject("currency");
    }

    async onChange(e) {
        let tmp_currency = e.path[0].value;
        await LocalStorageService.saveObject({"currency": tmp_currency})
        const currency = await LocalStorageService.getObject("currency");
        document.getElementsByTagName("lum-popup")[0].setAttribute("refresh-currency", "true");
    }
}

window.customElements.define('lum-currency-select', CurrencyComponent);

