const HTML = `
<div style="height: 200px; text-align: center">
    <lum-loader id="lum-loader" lum-loader-width="50" lum-loader-margin-top="70"></lum-loader>
    <figure class="highcharts-figure" style="margin: 0 0 0 0; ">
        <div id="pie-chart" style="display: none"></div>
    </figure>
</div>
`
import BaseComponent from "../../base.component.js";
import LumNetworkService from "../../../service/lum-network/lum-network.service.js";
import ExchangeRateService from "../../../service/exchange-rate/exchange-rate.service.js";
import LocalStorageService from "../../../service/local-storage/local-storage.service.js";
import LoaderComponent from "../../loader/loader.component.js";

export default class ChartPieComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    config = {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            height: 200,
            spacing:[15, 15, 15, 15]
        },
        credits:{
            enabled:false
        },
        title: {
            text: "<lum-loader></lum-loader>",
            align: 'center',
            verticalAlign: 'middle',
            style:{
                fontSize: "0.9rem",
                fontFamily: "Work Sans",
                fontWeight: "600"

            },
            y: 15
        },
        tooltip: {
            pointFormat: 'Token : <b>{point.token}</b><br>Percentage : <b>{point.percentage:.1f}%</b><br>Balance : <b>{point.balance}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        exporting: {
            enabled:false,
            accessibility: {
                enabled: false
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false
                },
                linecap: 'round',
                stickyTracking: false,
                rounded: true,
                startAngle: -180,
                endAngle: 180,
                size: '130%'
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            innerSize: '85%',
            size: '100%',
            zIndex: 1000,
            data: [
                {name:'Unbounded',y: 0,token:0,color:"#5f99dc",balance:""},
                {name:'Available',y: 0,token:0,color:"#5fd68b",balance:""},
                {name:'Delegate - Available',y: 0,token:0,color:"#fd9033",balance:""},
                {name:'Delegate - Vesting',y: 0,token:0,color:"#fd9033",balance:""},
                {name:'Reward',y: 0,token:0,color:"#5fd2dc",balance:""},
                {name:'Vesting',y: 0,token:0,color:"#747c88",balance:""},
            ]
        }]
    };


    constructor() {
        super();
        this.template.innerHTML = ChartPieComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.loadLumUserChartInfo();
    }

    async loadLumUserChartInfo() {
        const lumWalletAddress = await LocalStorageService.getObject("lum_address");
        const currency = await LocalStorageService.getObject("currency");
        const LumModel = await LumNetworkService.getLumInfo();
        const LumUserWalletModel = await LumNetworkService.getLumUserWalletInfo(lumWalletAddress);
        const ExchangeRateModel = await ExchangeRateService.getExchangeRate(currency);
        await this.render(LumUserWalletModel, ExchangeRateModel, LumModel);
    }

    render(LumUserWalletModel, ExchangeRateModel, LumModel){
        this.config.series[0].data[0].y = parseFloat(LumUserWalletModel.unboundedLumToken * 100 / LumUserWalletModel.allLumToken);
        this.config.series[0].data[1].y = parseFloat(LumUserWalletModel.availableLumToken * 100 / LumUserWalletModel.allLumToken);
        this.config.series[0].data[2].y = parseFloat((LumUserWalletModel.delegateLumToken - LumUserWalletModel.vestingLumToken.locked_delegated_coins) * 100 / LumUserWalletModel.allLumToken);
        this.config.series[0].data[3].y = parseFloat(LumUserWalletModel.vestingLumToken.locked_delegated_coins * 100 / LumUserWalletModel.allLumToken);
        this.config.series[0].data[4].y = parseFloat(LumUserWalletModel.rewardLumToken * 100 / LumUserWalletModel.allLumToken);
        this.config.series[0].data[5].y = parseFloat(LumUserWalletModel.vestingLumToken.locked_bank_coins * 100 / LumUserWalletModel.allLumToken);

        this.config.series[0].data[0].balance = this.formatPriceToDisplay(LumUserWalletModel.unboundedLumToken * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);
        this.config.series[0].data[1].balance = this.formatPriceToDisplay(LumUserWalletModel.availableLumToken * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);
        this.config.series[0].data[2].balance = this.formatPriceToDisplay((LumUserWalletModel.delegateLumToken - LumUserWalletModel.vestingLumToken.locked_delegated_coins) * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);
        this.config.series[0].data[3].balance = this.formatPriceToDisplay(LumUserWalletModel.vestingLumToken.locked_delegated_coins * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);
        this.config.series[0].data[4].balance = this.formatPriceToDisplay(LumUserWalletModel.rewardLumToken * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);
        this.config.series[0].data[5].balance = this.formatPriceToDisplay(LumUserWalletModel.vestingLumToken.locked_bank_coins * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);

        this.config.series[0].data[0].token = new Intl.NumberFormat(undefined, {})
            .format(LumUserWalletModel.unboundedLumToken
                .toFixed(2)
            );
        this.config.series[0].data[1].token = new Intl.NumberFormat(undefined, {})
            .format(LumUserWalletModel.availableLumToken
                .toFixed(2)
            );
        this.config.series[0].data[2].token = new Intl.NumberFormat(undefined, {})
            .format((LumUserWalletModel.delegateLumToken - LumUserWalletModel.vestingLumToken.locked_delegated_coins)
                .toFixed(2)
            );
        this.config.series[0].data[3].token = new Intl.NumberFormat(undefined, {})
            .format(LumUserWalletModel.vestingLumToken.locked_delegated_coins
                .toFixed(2)
            );
        this.config.series[0].data[4].token = new Intl.NumberFormat(undefined, {})
            .format(LumUserWalletModel.rewardLumToken
                .toFixed(2)
            );
        this.config.series[0].data[5].token = new Intl.NumberFormat(undefined, {})
            .format(LumUserWalletModel.vestingLumToken.locked_bank_coins
                .toFixed(2)
            );
        this.config.title.text = new Intl.NumberFormat(undefined, {}).format(LumUserWalletModel.allLumToken.toFixed(0))
            + " lum <br>"
            + this.formatPriceToDisplay(LumUserWalletModel.allLumToken * LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency,2);

        this.chart = Highcharts.chart(this.shadowRoot.getElementById("pie-chart"),this.config);
        this.shadowRoot.getElementById("lum-loader").style.display = 'none';
        this.shadowRoot.getElementById("pie-chart").style.display = 'block';
    }

    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.getElementById("lum-loader").style.display = 'block';
            this.shadowRoot.getElementById("pie-chart").style.display = 'none';
            this.loadLumUserChartInfo();
        }
    }

}

window.customElements.define('lum-chart-pie', ChartPieComponent);
