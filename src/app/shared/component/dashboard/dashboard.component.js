const HTML = `
<div class="row">
    <div class="col-5" >
        <lum-chart-pie></lum-chart-pie>
    </div>
    <div class="col-7">
        <lum-token-type-list></lum-token-type-list>
    </div>
</div>
`
import BaseComponent from "../base.component.js";
import ChartPieComponent from "./chart-pie/chart-pie.component.js";
import TokenTypeListComponent from "./token-type-list/token-type-list.component.js";

export default class DashboardComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        this.template.innerHTML = DashboardComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
    }

    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.querySelector("lum-chart-pie").setAttribute("refresh-all", "true");
            this.shadowRoot.querySelector("lum-token-type-list").setAttribute("refresh-all", "true");
        }
    }
}

window.customElements.define('lum-dashboard', DashboardComponent);
