const HTML = `
<div class ="row" id="main">
    <div class="col-5"><i id="icon-circle" style="" class="bi bi-circle-fill"></i><span id="title"></span></div>
    <div class="col-3" id="lum-number">
        <div id="lum-number-loader">
            <lum-loader lum-loader-width="15"></lum-loader>
        </div>
        <div id="lum-number-text">
            <span id="lum-number-formatted"></span><span></span>
        </div>
    </div>
    <div class="col-4" id="lum-balance">
        <div id="lum-balance-loader">
            <lum-loader lum-loader-width="15"></lum-loader>
        </div>
        <div id="lum-balance-text">
            <span id="lum-balance-formatted"></span>
        </div>
    </div>
</div>
<style>
#main { padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid lightgray; }
#icon-circle { font-size: 0.9em; }
#lum-number, #lum-balance { text-align: end; }
</style>
`
import BaseComponent from "../../../base.component.js";
import LoaderComponent from "../../../loader/loader.component.js";
import LocalStorageService from "../../../../service/local-storage/local-storage.service.js";
import ExchangeRateService from "../../../../service/exchange-rate/exchange-rate.service.js";
import LumNetworkService from "../../../../service/lum-network/lum-network.service.js";

export default class TokenDetailComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        this.template.innerHTML = TokenDetailComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.type = this.getAttribute('lum-token-type');
        this.shadowRoot.getElementById("title").innerHTML = " " + this.type.charAt(0).toUpperCase() + this.type.slice(1);
        this.shadowRoot.getElementById("icon-circle").style.color = this.getCircleColor(this.type);
        this.loadLumUserWalletInfo();
    }

    render(LumUserWalletModel, ExchangeRateModel, LumModel){
        let lumTokenCount = this.type === "vesting" ? LumUserWalletModel[this.type + "LumToken"].locked_bank_coins : LumUserWalletModel[this.type + "LumToken"];

        this.shadowRoot.getElementById("lum-balance-formatted").innerText = this.formatPriceToDisplay(lumTokenCount * LumModel.priceCurrent * ExchangeRateModel.exchangeRate,ExchangeRateModel.currency,2);
        this.shadowRoot.getElementById("lum-balance-loader").style.display = 'none';
        this.shadowRoot.getElementById("lum-balance-text").style.display = 'block';
        this.shadowRoot.getElementById("lum-number-formatted").innerText = new Intl.NumberFormat(undefined, {}).format(lumTokenCount.toFixed(2));
        this.shadowRoot.getElementById("lum-number-loader").style.display = 'none';
        this.shadowRoot.getElementById("lum-number-text").style.display = 'block';
    }

    async loadLumUserWalletInfo() {

        const lumWalletAddress = await LocalStorageService.getObject("lum_address");
        const currency = await LocalStorageService.getObject("currency");
        const LumModel = await LumNetworkService.getLumInfo();
        const LumUserWalletModel = await LumNetworkService.getLumUserWalletInfo(lumWalletAddress);
        const ExchangeRateModel = await ExchangeRateService.getExchangeRate(currency);
        this.render(LumUserWalletModel, ExchangeRateModel, LumModel);
    }

    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.getElementById("lum-number-loader").style.display = 'block';
            this.shadowRoot.getElementById("lum-number-text").style.display = 'none';
            this.shadowRoot.getElementById("lum-balance-loader").style.display = 'block';
            this.shadowRoot.getElementById("lum-balance-text").style.display = 'none';
            this.loadLumUserWalletInfo();
        }
    }

    getCircleColor(type){
        let color = "";
        switch (type){
            case 'available':
                color = "#5fd68b";
                break;
            case 'delegate':
                color = "#fd9033";
                break;
            case 'reward':
                color = "#5fd2dc";
                break;
            case 'unbounded':
                color = "#5f99dc";
                break;
            case 'vesting':
                color = "#747c88";
                break;
        }
        return color;
    }

}

window.customElements.define('lum-token-detail', TokenDetailComponent);
