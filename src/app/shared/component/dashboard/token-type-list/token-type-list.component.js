const HTML = `
<div>
   <lum-token-detail lum-token-type="available"></lum-token-detail>
   <lum-token-detail lum-token-type="delegate"></lum-token-detail>
   <lum-token-detail lum-token-type="reward"></lum-token-detail>
   <lum-token-detail lum-token-type="unbounded" id="test"></lum-token-detail>
   <lum-token-detail lum-token-type="vesting" id="test"></lum-token-detail>
</div>
<style>

</style>
`
import BaseComponent from "../../base.component.js";
import TokenDetailComponent from "./token-detail/token-detail.component.js";

export default class TokenTypeListComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        this.template.innerHTML = TokenTypeListComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));

    }

    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.querySelectorAll("lum-token-detail").forEach(function (elem){
                elem.setAttribute("refresh-all", "true");
            } );
        }
    }
}

window.customElements.define('lum-token-type-list', TokenTypeListComponent);
