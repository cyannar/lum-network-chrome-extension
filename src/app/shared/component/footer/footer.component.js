const HTML = `
<div class="row">
    <div class="col-1">
        <lum-refresh-button></lum-refresh-button>
    </div>
    <div class="col-3">
        <lum-currency-select></lum-currency-select>
    </div>
    <div class="col-5">
        <div>
            <a href="#" id="option-link" onclick="">Options</a>
        </div>
    </div>
    <div class="col-3">
        <lum-extension-version></lum-extension-version>
    </div>
</div>
<style>
#option-link {
    float: left;
    font-size: 12px;
    color: #9e9797;
    margin-top: 13px;
}
</style>`


import BaseComponent from "../base.component.js";
import CurrencyComponent from "../currency/currency.component.js";
import VersionComponent from "../version/version.component.js";
import RefreshButtonComponent from "../refresh-button/refresh-button.component.js";


export default class FooterComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap'
    ];

    constructor() {
        super();
        this.addEventListener('click',this.onClick)
        this.template.innerHTML = FooterComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.shadowRoot.getElementById("option-link").onclick = e => chrome.runtime.openOptionsPage();
    }

}

window.customElements.define('lum-footer', FooterComponent);

