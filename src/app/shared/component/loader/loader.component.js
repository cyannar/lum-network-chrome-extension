const HTML = `
<div class="spinner-border" role="status" id="lum-loader" style="vertical-align: center;">
    <span class="visually-hidden">Loading...</span>
</div>
<style>
#lum-loader{
color: #bbc2c3;
}
</style>
`
import BaseComponent from "../base.component.js";


export default class LoaderComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    static attributes = [];

    constructor() {
        super();
        this.template.innerHTML = LoaderComponent.getStyleSheets() + HTML;

    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        let width = this.getAttribute('lum-loader-width');
        this.shadowRoot.getElementById("lum-loader").style.width = width + 'px';
        this.shadowRoot.getElementById("lum-loader").style.height = width + 'px';
        let marginTop = this.getAttribute('lum-loader-margin-top');
        this.shadowRoot.getElementById("lum-loader").style.marginTop = marginTop + 'px';
        let color = this.getAttribute('lum-loader-color');
        this.shadowRoot.getElementById("lum-loader").style.color = color + '';
    }
}

window.customElements.define('lum-loader', LoaderComponent);
