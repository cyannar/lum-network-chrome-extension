const HTML = `
<div style="height: 100px; text-align: center">
    <lum-loader id="lum-loader" lum-loader-width="50" lum-loader-margin-top="50"></lum-loader>
    <div id="lum-chart" style="display: none"></div>
</div>
`
import BaseComponent from "../base.component.js";
import LumNetworkService from "../../service/lum-network/lum-network.service.js";
import ExchangeRateService from "../../service/exchange-rate/exchange-rate.service.js";
import LocalStorageService from "../../service/local-storage/local-storage.service.js";
import LoaderComponent from "../loader/loader.component.js";

export default class LumChartComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    config = {
        exporting: {
            enabled:false,
            accessibility: {
                enabled: false
            }
        },
        chart: {
            height: 140,
            type: 'area',
            spacingLeft: 1,
            spacingRight: 1,
            spacingTop: 30,
            spacingBottom: 1

        },
        navigator: {
            enabled:false,
            series: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        scrollbar: {
            enabled:false
        },
        rangeSelector: {
            enabled:false
        },
        xAxis: {
            type: 'datetime',
            labels: {
                format: "{value:%b %d}",
                title: {
                    enabled: false
                },
            }
        },
        yAxis: {
            title: {
                text: null
            },
            endOnTick:false,
            opposite:false,
            min: 0,
            tickInterval:0.001
        },
        tooltip: {
            valueSuffix: ' $US',
            xDateFormat: '%A, %b %e'
        },
        title: {
            text: undefined
        },
        credits:{
            enabled:false
        },
        series: [{
            name: "lum price",
            data: [],
            tooltip: {
                headerFormat: "{point.x:%b %d %H:%M}<br>",
            },
            showInLegend:false,
            softThreshold: false
        }]
    };


    constructor() {
        super();
        this.template.innerHTML = LumChartComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.loadLumUserChartInfo();
    }

    async loadLumUserChartInfo() {
        const currency = await LocalStorageService.getObject("currency");
        const ExchangeRateModel = await ExchangeRateService.getExchangeRate(currency);
        const LumChartModel = await LumNetworkService.getLumChartData(7);
        await this.render(LumChartModel, ExchangeRateModel);
    }

    render(LumChartModel, ExchangeRateModel){

        let data = [];
        if(ExchangeRateModel.currency === "EUR"){
            this.config.tooltip.valueSuffix = ' &euro;';
        }
        if(ExchangeRateModel.currency === "USD"){
            this.config.tooltip.valueSuffix = ' $US';
        }

        LumChartModel.chartData.forEach(function(raw){
            data.push({
                x: raw.time,
                y: parseFloat((raw.price * ExchangeRateModel.exchangeRate).toFixed(5))
            });
        });

        this.config.yAxis.min = LumChartModel.minPrice * ExchangeRateModel.exchangeRate;
        this.config.series[0].data = data;
        this.chart = Highcharts.chart(this.shadowRoot.getElementById("lum-chart"),this.config);
        this.shadowRoot.getElementById("lum-loader").style.display = 'none';
        this.shadowRoot.getElementById("lum-chart").style.display = 'block';
    }

    attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.getElementById("lum-loader").style.display = 'block';
            this.shadowRoot.getElementById("lum-chart").style.display = 'none';
            this.loadLumUserChartInfo();
        }
    }

}

window.customElements.define('lum-chart', LumChartComponent);
