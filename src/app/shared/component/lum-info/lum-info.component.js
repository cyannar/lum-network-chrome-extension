const HTML = `
<div class="row" id="lum-price">
    <div class="col-6" style="border-right: 1px solid lightgray;">
        <div class="card text-dark bg-light" id="lum-price-card">
            <div class="card-body">
                <h5 class="card-title">
                    lum
                    <a href="https://info.osmosis.zone/token/LUM" target="_blank" style="float: right">
                        <i style="font-size: 0.9em;" class="bi bi-box-arrow-up-right text-dark"></i>
                    </a>
                </h5>
                <div class="card-text">
                    <div id="lum-price-loader" style="height: 37.5px"> 
                        <lum-loader lum-loader-width="20" lum-loader-margin-top="10"></lum-loader>
                    </div>
                    <div class ="number-text" id="lum-price-text">
                        <span id="lum-price-formatted"></span> <span id="lum-growth-rate-formatted"></span>
                    </div>
                    <span>
                        <div style="font-size: 15px;">
                            <small>
                                <span><b>Volume (24h) :</b></span>
                                <span id="lum-volume-formatted" style="display: none"></span>
                                <span id="lum-volume-loader">
                                    <lum-loader lum-loader-width="15"></lum-loader>
                                </span>
                            </small>
                        </div>
                    </span>
                    <span>
                        <div style="font-size: 15px;">
                            <small>
                                <span><b>Liquidity :</b></span>
                                <span id="lum-liquidity-formatted" style="display: none"></span>
                                <span id="lum-liquidity-loader">
                                    <lum-loader lum-loader-width="15" ></lum-loader>
                                </span>
                            </small>
                        </div>  
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <lum-chart></lum-chart>
    </div>
</div>
<style>
#lum-price-formatted{
font-size: 0.7em;
    font-weight: bold;
}
#lum-growth-rate-formatted{
    font-size: 0.7em;
    
}
</style>
`

import BaseComponent from "../base.component.js";
import LoaderComponent from "../loader/loader.component.js";
import LumChartComponent from "../lum-chart/lum-chart.component.js";
import LumNetworkService from "../../service/lum-network/lum-network.service.js";
import LocalStorageService from "../../service/local-storage/local-storage.service.js";
import ExchangeRateService from "../../service/exchange-rate/exchange-rate.service.js";

export default class LumInfoComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup',
    ];

    constructor() {
        super();
        constructor
        this.template.innerHTML = LumInfoComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.render();
    }

    async render() {
        const LumModel = await LumNetworkService.getLumInfo();
        const currency = await LocalStorageService.getObject("currency");
        const ExchangeRateModel = await ExchangeRateService.getExchangeRate(currency);
        this.shadowRoot.querySelector("#lum-price-formatted").innerHTML = this.formatPriceToDisplay(LumModel.priceCurrent * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency, 4);
        this.shadowRoot.querySelector("#lum-price-text").style.display = 'block';
        this.shadowRoot.querySelector("#lum-price-loader").style.display = 'none';
        this.shadowRoot.querySelector("#lum-liquidity-formatted").innerHTML = this.formatPriceToDisplay(LumModel.liquidity * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency, 0);
        this.shadowRoot.querySelector("#lum-liquidity-formatted").style.display = 'contents';
        this.shadowRoot.querySelector("#lum-liquidity-loader").style.display = 'none';
        this.shadowRoot.querySelector("#lum-volume-formatted").innerHTML = this.formatPriceToDisplay(LumModel.volume * ExchangeRateModel.exchangeRate, ExchangeRateModel.currency, 0);
        this.shadowRoot.querySelector("#lum-volume-formatted").style.display = 'contents';
        this.shadowRoot.querySelector("#lum-volume-loader").style.display = 'none';
        let grow_rate_text = "";
        let color = "";
        if (LumModel.growthRate > 0) {
            color = "#5f9d5f";
            grow_rate_text = "+"
        } else if (LumModel.growthRate < 0) {
            color = "#ff3131";
        }
        this.shadowRoot.getElementById("lum-growth-rate-formatted").style.color = color;
        this.shadowRoot.getElementById("lum-growth-rate-formatted").innerHTML = "(" + grow_rate_text + LumModel.growthRate.toFixed(2) + "%)";
    }

    async attributeChangedCallback(attr, oldValue, newValue) {
        if (attr === "refresh-all") {
            this.shadowRoot.querySelector("lum-chart").setAttribute("refresh-all", "true");
            this.shadowRoot.querySelector("#lum-price-text").style.display = 'none';
            this.shadowRoot.querySelector("#lum-price-loader").style.display = 'block';
            this.shadowRoot.querySelector("#lum-volume-formatted").style.display = 'none';
            this.shadowRoot.querySelector("#lum-volume-loader").style.display = 'contents';
            this.shadowRoot.querySelector("#lum-liquidity-formatted").style.display = 'none';
            this.shadowRoot.querySelector("#lum-liquidity-loader").style.display = 'contents';
            this.render();
        }
    }
}

window.customElements.define('lum-info', LumInfoComponent);
