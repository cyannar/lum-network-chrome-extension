const HTML = `
<button class="btn btn-outline-secondary btn-sm">
    <i class="bi bi-arrow-repeat"></i>
</button>
`

import BaseComponent from "../base.component.js";

export default class RefreshButtonComponent extends BaseComponent {

    static styleSheets = [
        'bootstrap',
        'popup'
    ];

    constructor() {
        super();
        this.addEventListener('click',this.onClick)
        this.template.innerHTML = RefreshButtonComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));

    }

    onClick() {
        document.getElementsByTagName("lum-popup")[0].setAttribute("refresh-all","true");
    }
}

window.customElements.define('lum-refresh-button', RefreshButtonComponent);

