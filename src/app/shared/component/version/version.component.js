// créer une class qui extend HTMLElement
const HTML = `
<div id = 'lum-extension-version'></div>
<style> 
    #lum-extension-version { 
        float: right; 
        font-size: 12px; 
        color: #9e9797; 
        margin-top: 13px; 
    }
</style>`
import BaseComponent from "../base.component.js";


export default class VersionComponent extends BaseComponent {

    static styleSheets = [];

    constructor() {
        super();
        this.template.innerHTML = VersionComponent.getStyleSheets() + HTML;
    }

    connectedCallback() {
        const root = this.attachShadow({mode: 'open'});
        root.appendChild(this.template.content.cloneNode(true));
        this.loadVersion()
    }

    async render(version) {
        const lum_extension_version = this.shadowRoot.querySelector('#lum-extension-version');
        lum_extension_version.insertAdjacentHTML('afterbegin', "Version " + version);
    }

    async loadVersion() {
        const data = await fetch("/manifest.json").then(response => response.json());
        await this.render(data.version);
    }
}



window.customElements.define('lum-extension-version', VersionComponent);

