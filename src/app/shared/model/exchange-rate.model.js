export default class ExchangeRateModel {
    currency;
    exchangeRate;

    constructor(exchangeRateRawData,currency){
        this.currency = currency;
        this.exchangeRate = exchangeRateRawData.rates[currency];
    }

}
