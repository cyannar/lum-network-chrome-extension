export default class LumChartModel {
    chartData;
    minPrice;

    constructor(lumChartRawData){
        let data = [];
        let minPrice = 0;
        lumChartRawData.forEach(function(raw){
            data.push({
                time: raw.time*1000,
                price: raw.close
            });
            if(raw.close < minPrice || minPrice === 0){
                minPrice = raw.close;
            }
        });
        this.minPrice = minPrice;
        this.chartData = data;
    }

}
