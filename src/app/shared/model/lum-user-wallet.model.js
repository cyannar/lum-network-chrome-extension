export default class LumUserWalletModel {
    lumWalletAddress;
    allLumToken;
    availableLumToken;
    unboundedLumToken;
    vestingLumToken;
    delegateLumToken;
    rewardLumToken;

    constructor(lumUserWalletRawData, lumWalletAddress){
        let unbounded_lum = 0;
        let delegated_lum = 0;

        lumUserWalletRawData.result.unbondings.forEach(function(unbounded){
            unbounded.entries.forEach(function(entrie){
                unbounded_lum = unbounded_lum + (entrie.balance / 1000000);
            });
        });
        lumUserWalletRawData.result.delegations.forEach(function(delegation){
            delegated_lum = delegated_lum + (delegation.balance.amount / 1000000);
        });



        this.unboundedLumToken = unbounded_lum;
        this.delegateLumToken = delegated_lum;
        this.lumWalletAddress = lumWalletAddress;
        this.availableLumToken = lumUserWalletRawData.result.balance.amount / 1000000;
        this.rewardLumToken = lumUserWalletRawData.result.all_rewards.total[0].amount / 1000000000000000000000000;

        console.log()


        this.vestingLumToken = {
            locked_bank_coins: lumUserWalletRawData.result.vesting === null ? 0 : lumUserWalletRawData.result.vesting.locked_bank_coins.amount / 1000000,
            locked_delegated_coins: lumUserWalletRawData.result.vesting === null ? 0 : lumUserWalletRawData.result.vesting.locked_delegated_coins.amount / 1000000,
            locked_coins: lumUserWalletRawData.result.vesting === null ? 0 : lumUserWalletRawData.result.vesting.locked_coins.amount / 1000000,
            unlocked_coins: lumUserWalletRawData.result.vesting === null ? 0 : lumUserWalletRawData.result.vesting.unlocked_coins.amount / 1000000,
        };
        this.allLumToken = this.unboundedLumToken + this.delegateLumToken + this.availableLumToken + this.rewardLumToken + this.vestingLumToken.locked_bank_coins
    }
}
