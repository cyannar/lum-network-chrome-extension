export default class LumModel {
    priceCurrent;
    priceLastDay;
    growthRate;
    liquidity;
    volume;

    constructor(lumRawData){
        this.priceCurrent = lumRawData.result.price;
        this.priceLastDay = lumRawData.result.previous_day_price;
        this.liquidity = lumRawData.result.liquidity;
        this.volume = lumRawData.result.volume_24h;
        this.growthRate = ((this.priceCurrent - this.priceLastDay) / this.priceLastDay) * 100;
    }

}
