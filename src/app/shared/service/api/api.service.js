export default class ApiService {

    static async makeHttpRequest(httpMethod, url) {
        let config = {
            method : httpMethod
        }
        let request = new Request(url,config);
        return fetch(request);
    };
}
