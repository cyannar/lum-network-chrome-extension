import LocalStorageService from "../local-storage/local-storage.service.js";
import LumNetworkService from "../lum-network/lum-network.service.js";
import NotificationService from "../notification/notification.service.js";


export default class DashboardService {
    constructor() {
        this.addListeners();
    }

    addListeners() {
        this.addAlarms();
        this.addEventOnAlarm();
    }

    addAlarms() {
        chrome.alarms.create('alarm_each_5_minutes', {
            periodInMinutes: 5,
            delayInMinutes: 0
        });
    }

    addEventOnAlarm() {
        chrome.alarms.onAlarm.addListener((alarm) => {
            if (alarm.name === "alarm_each_5_minutes") {
                this.processRewardsNotification();
            }
        });
    }

    async processRewardsNotification() {
        const notif_limit_rewards = await LocalStorageService.getObject("notif-limit-rewards");
        if (notif_limit_rewards) {
            const lum_address = await LocalStorageService.getObject("lum_address");
            const notif_limit_rewards_number = await LocalStorageService.getObject("notif-limit-rewards-number");
            const LumUserWalletInfoModel = await LumNetworkService.getLumUserWalletInfo(lum_address);
            if (LumUserWalletInfoModel.rewardLumToken > notif_limit_rewards_number) {
                const notif_already_sent = await LocalStorageService.getObject("notif_already_sent");
                let days = 0;
                if (await LocalStorageService.getObject("notif_sent_rewards_date") !== undefined) {
                    let notif_last_send_date = await LocalStorageService.getObject("notif_sent_rewards_date");
                    notif_last_send_date = new Date(notif_last_send_date);
                    const date_today = new Date(this.getDate());
                    days = parseInt((date_today - notif_last_send_date) / (1000 * 60 * 60 * 24))
                }
                if (!notif_already_sent || (notif_already_sent && days >= 1)) {
                    let msg = 'You have ' + LumUserWalletInfoModel.rewardLumToken.toFixed(2) + ' lums in reward. You can claim their.'
                    let title = 'Lum rewards';
                    let notification = new NotificationService('basic', title, msg);
                    notification.send();
                    await LocalStorageService.saveObject({"notif_sent_rewards_date": this.getDate()});
                    await LocalStorageService.saveObject({"notif_already_sent": true});
                }
            } else {
                await LocalStorageService.saveObject({"notif_already_sent": false});
            }
        } else {
            await LocalStorageService.saveObject({"notif_already_sent": false});
        }
    }

    getDate() {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }

        var d = new Date();
        return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
    }
}
