import ApiService from '/src/app/shared/service/api/api.service.js'
import ExchangeRateModel from "../../model/exchange-rate.model.js";

export default class ExchangeRateService {
    static API_URL = "https://api.exchangerate-api.com/v4/latest/USD";

    static async getExchangeRate(currency) {
        if(currency === undefined) {
            currency = "USD";
        }

        if(this.responseApiService === undefined){
            this.responseApiService = ApiService.makeHttpRequest("GET",  this.API_URL);
        }
        let json = {};
        await this.responseApiService.then(async function (response){
            await response.clone().json().then(async function(json_tmp) {
                json = json_tmp
            });
        });
        return new ExchangeRateModel(json,currency);


    }

    static clear(){
        this.promiseApiService = undefined;
    }

}
