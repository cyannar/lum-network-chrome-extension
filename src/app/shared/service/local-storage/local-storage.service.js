export default class LocalStorageService {

    static async getObject(key) {
        return new Promise((resolve, reject) => {
            try {
                chrome.storage.sync.get(key, function(value) {
                    resolve(value[key]);
                });
            } catch (ex) {
                reject(ex);
            }
        });
    };

    static async saveObject(obj) {
        return new Promise((resolve, reject) => {
            try {
                chrome.storage.sync.set(obj, function() {
                    resolve();
                });
            } catch (ex) {
                reject(ex);
            }
        });
    };

    static async clear() {
        return new Promise((resolve, reject) => {
            try {
                chrome.storage.sync.clear(function() {
                    resolve();
                });
            } catch (ex) {
                reject(ex);
            }
        });
    };
}
