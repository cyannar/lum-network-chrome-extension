import ApiService from '/src/app/shared/service/api/api.service.js'
import LumModel from '/src/app/shared/model/lum.model.js'
import LumChartModel from '/src/app/shared/model/lum-chart.model.js'
import LumUserWalletModel from '/src/app/shared/model/lum-user-wallet.model.js'

export default class LumNetworkService {
    static API_URL_LUM_NETWORK = "https://bridge.mainnet.lum.network";
    static API_URL_OSMOSIS = "https://api-osmosis.imperator.co/tokens/v1/historical";

    static async getLumInfo() {
        let route = "/lum";
        if(this.responseApiServiceLumInfo === undefined){
            this.responseApiServiceLumInfo = ApiService.makeHttpRequest("GET", this.API_URL_LUM_NETWORK + route);
        }
        let json = {};
        await this.responseApiServiceLumInfo.then(async function (response){
            await response.clone().json().then(async function(json_tmp) {
                json = json_tmp
            });
        });
        let tmp_return = undefined;

        if(json.code && json.code === 200){
            tmp_return = new LumModel(json);
        }
        return tmp_return;
    }

    static async getLumUserWalletInfo(lum_wallet_address) {
        let route = "/accounts/" + lum_wallet_address;

        if(this.responseApiServiceUserWalletInfo === undefined){
            this.responseApiServiceUserWalletInfo = ApiService.makeHttpRequest("GET", this.API_URL_LUM_NETWORK + route);
        }
        let json = {};
        await this.responseApiServiceUserWalletInfo.then(async function (response){
            await response.clone().json().then(async function(json_tmp) {
                json = json_tmp
            });
        });
        let tmp_return = undefined;

        if(json.code && json.code === 200){
            tmp_return = new LumUserWalletModel(json,lum_wallet_address);
        }
        return tmp_return;
    }

    static async getLumChartData(day = 3) {
        let route = "/LUM/chart";
        let query_param = "?range=" + day + "d";

        if(this.responseApiServiceLumChart === undefined){
            this.responseApiServiceLumChart = ApiService.makeHttpRequest("GET", this.API_URL_OSMOSIS + route + query_param);
        }
        let json = {};
        await this.responseApiServiceLumChart.then(async function (response){
            await response.clone().json().then(async function(json_tmp) {
                json = json_tmp
            });
        });
        return new LumChartModel(json);
    }

    static clear(){
        this.responseApiServiceLumChart = undefined;
        this.responseApiServiceUserWalletInfo = undefined;
        this.responseApiServiceLumInfo = undefined;
    }

}
