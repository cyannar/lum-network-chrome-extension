
export default class NotificationService {
    type;
    title;
    message;

    constructor(type, title, message) {
        this.type = type;
        this.title = title;
        this.message = message;
    }

    send(){
        chrome.notifications.create('NOTFICATION_ID', {
            type: this.type,
            iconUrl: '../../assets/image/lum_logo.png',
            title: this.title,
            message: this.message,
            priority: 2
        });
    }


}
